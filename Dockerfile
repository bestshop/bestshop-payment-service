FROM java:8-jre-alpine

EXPOSE 8080

ADD ./target/bestshop-payment-service.jar /app/

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/bestshop-payment-service.jar"]