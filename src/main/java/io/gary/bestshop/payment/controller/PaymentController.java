package io.gary.bestshop.payment.controller;

import io.gary.bestshop.payment.domain.Payment;
import io.gary.bestshop.payment.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("payments")
public class PaymentController {

    private final PaymentService paymentService;

    @GetMapping
    public List<Payment> getPayments() {
        return paymentService.getPayments();
    }

    @PostMapping
    public Payment createPayment(@RequestBody @Valid Payment payment) {
        return paymentService.createPayment(payment);
    }

    @GetMapping("{id}")
    public Payment getPayment(@PathVariable("id") String id) {
        return paymentService.getPayment(id);
    }
}
