package io.gary.bestshop.payment.service;

import io.gary.bestshop.payment.domain.Payment;
import io.gary.bestshop.payment.errors.PaymentNotFoundException;
import io.gary.bestshop.payment.messaging.PaymentEventPublisher;
import io.gary.bestshop.payment.repository.PaymentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class PaymentService {

    private final PaymentRepository paymentRepository;

    private final PaymentEventPublisher paymentEventPublisher;

    public List<Payment> getPayments() {

        log.info("Getting payments");

        return paymentRepository.findAll();
    }

    public Payment getPayment(@NotNull String id) {

        log.info("Getting payment: id={}", id);

        return findPaymentOrThrow(id);
    }

    public Payment createPayment(@NotNull @Valid Payment payment) {

        log.info("Creating payment: payment={}", payment);

        Payment createdPayment = paymentRepository.save(payment.withCreatedAt(LocalDateTime.now()));

        return paymentEventPublisher.publishPaymentReceivedEvent(createdPayment);
    }

    private Payment findPaymentOrThrow(String id) {
        return paymentRepository.findById(id).orElseThrow(() -> new PaymentNotFoundException(id));
    }
}
