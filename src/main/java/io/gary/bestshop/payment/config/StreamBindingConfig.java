package io.gary.bestshop.payment.config;

import io.gary.bestshop.payment.messaging.MessagingChannels;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding(MessagingChannels.class)
public class StreamBindingConfig {

}
