package io.gary.bestshop.payment.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Wither;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Wither
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "payments")
public class Payment {

    @Id
    @JsonIgnoreProperties(allowGetters = true)
    private String id;

    @NotBlank
    private String orderId;

    @NotBlank
    @Pattern(regexp = "\\d{15,16}")
    private String instrument;

    @NotNull
    private BigDecimal amount;

    @NotNull
    private PaymentStatus status;

    private LocalDateTime createdAt;
}
