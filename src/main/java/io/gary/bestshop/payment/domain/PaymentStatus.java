package io.gary.bestshop.payment.domain;

public enum PaymentStatus {

    Successful,
    Failed
}
