package io.gary.bestshop.payment.messaging;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface MessagingChannels {

    String PAYMENT_RECEIVED_OUTPUT = "paymentReceivedOutput";

    @Output(PAYMENT_RECEIVED_OUTPUT)
    MessageChannel paymentReceivedOutput();

}
