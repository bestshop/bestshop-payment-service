package io.gary.bestshop.payment.messaging;

import io.gary.bestshop.messaging.dto.PaymentDto;
import io.gary.bestshop.messaging.event.payment.PaymentReceivedEvent;
import io.gary.bestshop.payment.domain.Payment;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import static org.springframework.integration.support.MessageBuilder.withPayload;

@Component
@RequiredArgsConstructor
public class PaymentEventPublisher {

    private final MessageChannel paymentReceivedOutput;

    public Payment publishPaymentReceivedEvent(Payment payment) {

        paymentReceivedOutput.send(
                withPayload(new PaymentReceivedEvent(toDto(payment))).build()
        );
        return payment;
    }

    private PaymentDto toDto(Payment payment) {
        return PaymentDto.builder()
                .id(payment.getId())
                .orderId(payment.getOrderId())
                .instrument(payment.getInstrument())
                .amount(payment.getAmount())
                .status(payment.getStatus().name())
                .createdAt(payment.getCreatedAt())
                .build();
    }
}
